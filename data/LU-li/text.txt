############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Luka tatie/ブランク L
@setface Sylph tatie/ブランク L
@setface Lilim tatie/ブランク L
@setface Lilith tatie/ブランク L

@mes 地の文1
@mes Luka
Huaa! I can't endure this!

@toki-ef 2 0
@mes 地の文1
Luka is being raped by Lilith and Lilim!
@mes Lilith
You already came? Too easy.
@mes Lilim
Huhu... Sister, he fainted on us. That's so pitiful.
@mes Sylph
Waaa! Luka! Wake up!
@mes Lilim
Look sister, it's the spirit of wind.
@mes Lilith
Yes sister... It's time to make ourselves forget about these 
last 500 years of disgrace.
@mes Lilim
That's right, sister...

@se sound/power03.mp3

@mes 地の文1
Lilith and Lilim chant a strange spell, creating a magic circle
on the ground!

@mes Sylph
...!? What are you doing!?

@se sound/power36.mp3
#画面エフェクト
#0フラッシュ　1黒フラッシュ　2射精　3画面振動　
#4完全暗転 （第二変数）は　0暗転　1白転　10立ち絵が見える暗転　-1解除　＋フレーム数
#5どきどき　6夜シーン用灯り表現　7画面RGB調整　8時間経過演出
#9長めフレームトランジション　10EV関連ピクチャ全消し
#11 ピクチャズーム P番　拡大率　フレーム
#12 選択肢前注意　13　イベント用天候変え　時間帯　0屋内1屋外
@toki-ef 0 70


@mes 地の文1
Light begins pouring out of the magic circle!
@mes Sylph
Hyaa! It's so bright!

@se sound/power11.mp3
@common_ev 500281
@picture LU-li\li01.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 2
@wait 70
@mes Sylph
...huh?
@mes 地の文1
Once the intense light dissipates, Sylph finds herself caught 
in Lilim's mouth.
@mes Sylph
What... how? How can you capture a spirit against her will...

#アニメ変数
@val_set 2500000 int 1
@mes Lilim
Mmmn...\i[001]Mmngh... It looks like it worked, sister.


@mes Lilith
Good job Lilim... Now, before she can escape... Finish it.

@mes Sylph
No, stop! Let me go! How...!
@mes Lilith
We haven't been doing nothing for these last 500 years while we
were sealed.
@mes Lilim
We researched ways of stealing the power of the spirits, 
Heinrich's source of strength.
Let us demonstrate what we've learned...

@mes Sylph
Oooh... Ha!
@val_set 2500000 int 0
@mes 地の文1
@toki-shake 40300 40350 05 42
@se sound/kucha03.mp3
@mes 地の文1
Sylph tries to escape, but the power of the wind doesn't work.
It appears that not even the spirit herself can use her own 
power.
@mes Sylph
No way! How is this possible!?

@mes Lilith
It's pointless. Your powers won't work inside this magic circle.
@mes Lilim
And now... It's time for the main event... Hehe.
@mes Lilith
This is wonderful, Lilim. Soon the powers of the spirits will 
be ours to command.
@mes Lilim
I can't wait any longer, sister. Let's hurry up and begin.
@common_ev 500281
@val_set 2500000 int 1
@picture LU-li\li02.jpg 0 480 360 10
@wait 70
@mes 地の文1
With Sylph in her mouth, Lilim joins hands with Lilith, and the
two bring their faces close to one another.
@mes Sylph
Heeey, no, don't! What are you doing? Stop!
The world's going to be in big trouble if something happens to 
me!
@mes Lilith
Huhu... We know already.
@mes Lilim
Specifically, it would be very dangerous if the power of wind 
were disrupted.
@mes Sylph
No way... What are you going to do to me?
@mes Lilim
What indeed... You're about to find out.
@mes Lilith
Ready Lilim?
@mes Lilim
Sister... Mmmn...

@se sound/lip00.mp3
@common_ev 500281
@picture LU-li\li03.jpg 0 480 360 10
@wait 30
@common_ev 500281
@picture LU-li\li04.jpg 0 480 360 10

@bgs sound/feratyupa2.mp3
@wait 70

@val_set 2500000 int 2
@mes Sylph
Nnngh... Ngh〜〜〜!
@mes 地の文1
Lilith and Lilim kiss with Sylph caught between their mouths. 
The succubus sisters begin coiling their tongues around her!

@mes Sylph
St... ngh... Stop... it!
@se sound/lip01.mp3
@toki-shake 40300 40350 05 42
@mes Lilith
Mmmn... Chp... Lilim... Sspst...
@se sound/lip00.mp3
@toki-shake 40300 40350 05 42
@mes Lilim
Mmngh, sister... Sspspst...
@se sound/eat1.mp3

@toki-shake 40300 40350 05 42
@common_ev 500281
@picture LU-li\li05.jpg 0 480 360 10
@wait 70
@mes Sylph
Ah, aah... What the... 
My body... It's melting...
@mes 地の文1
Sylph's body is smeared with saliva as tongues lick all over 
her.

@val_set 2500000 int 2
@mes 地の文1
No matter how she moves to try and escape, the succubi's 
tongues skillfully wrap up her body, both physically and with 
pleasure.
@mes Lilith
Mmmngh... And now like this...
@se sound/lip00.mp3
@toki-shake 40300 40350 05 42
@mes Lilim
Chp... Your power...
@toki-shake 40300 40350 05 42
@se sound/lip01.mp3
@mes Lilith
We'll melt and absorb it...♪
@mes Lilim
And then... We'll... Ngh... Nnngh...
@toki-shake 40300 40350 05 42
@se sound/lip00.mp3
@mes Lilith
The power of wind itself... 
Mmmngh... Uhuhu... We'll take it all...
@mes Lilim
And... Mmmpch...
The two of us... will become a new Sylph...
@mes Sylph
No... Aaah... That's crazy...
@se sound/eat1.mp3
@common_ev 500281
@picture LU-li\li05.jpg 0 480 360 10
@wait 70

@mes 地の文1
The two powerful succubi aren't just licking Sylph.
They're gradually sucking all the power out of her body through
their combined energy drain.
@mes Lilith
So this is the spirits' power... It's so delicious.
@mes Lilim
It's delicious, sister... 
Mine is mixing together with you, sister... Nngh...
@toki-shake 40300 40350 05 42
@se sound/lip01.mp3
@mes Lilith
Nnngh... Keep melting... Become one with us...
@mes 地の文1
The two succubi coil their tongues around more intensely, 
mercilessly violating Sylph's small body.

@mes Sylph
No! Stop...! Someone... help me...
@mes 地の文1
Strength drains from Sylph's body.
Her spirit energy is converted to syrupy liquid and greedily 
drank by the sisters.

@se sound/eat1.mp3
@common_ev 500281
@common_ev 500281
@picture LU-li\li06.jpg 0 480 360 10
@wait 70
@mes Sylph
No... I'm... melting...
@toki-shake 40300 40350 05 42
@se sound/lip00.mp3
@mes Lilim
Ngh... mmmngh...
@se sound/lip01.mp3
@toki-shake 40300 40350 05 42
@mes Lilith
Mmmpch...
@se sound/lip01.mp3
@toki-shake 40300 40350 05 42
@mes Sylph
N...o... Nnngh!
@mes 地の文1
The pair of tongues entwine and become one.
Sylph is tightly constrained in the tongue, and unable to move,
is melted like a piece of candy.
@mes Sylph
Don't... I can't...
@se sound/juru.mp3
@common_ev 500281
@picture LU-li\li07.jpg 0 480 360 10
@wait 70
@mes Lilith
Sspst... Hammmngh... Lilim...\i[001]

@val_set 2500000 int 3
@toki-shake 40300 40350 05 42
@mes Lilim
Mmmngh... Sister...\i[001]
@mes 地の文1
While embracing one another, the sisters move their tongues 
more vigorously, coating the spirit in saliva and melting her 
body.


@mes 地の文1
For a long time, her power is drained, little by little, and 
absorbed by the succubi.
@mes Sylph
Ha... aah...
@se sound/lip01.mp3
@toki-shake 40300 40350 05 42
@mes Lilith
Ngh... Nnngh...\i[001]
@se sound/lip00.mp3
@toki-shake 40300 40350 05 42
@mes Lilim
Ngh...\i[001]
@se sound/lip01.mp3
@toki-shake 40300 40350 05 42
@mes Sylph
.....
@mes 地の文1
Even though Sylph has already lost consciousness and can no 
longer resist, the two don't stop licking, melting, and 
drinking the spirit's energy.
@se sound/juru.mp3
@toki-shake 40300 40350 05 42
@mes Lilith
Mmmn...\i[001]
@mes Lilith
Ngh... Don't stop now, Lilim...\i[001]
@mes Lilith
Nnngh... Yes, sister...\i[001]
@se 12

@toki-ef 4 0 10
#ウェイト
@wait 70

@mes 地の文1
With Sylph tucked between their tongues, the sisters continue 
all day.
@toki-ef 4 -1 10
@val_set 2500000 int 1
@common_ev 500281
@picture LU-li\li08.jpg 0 480 360 10
@se sound/lick_s5.mp3
@wait 120

@mes 地の文1
Finally, they realize that the tiny spirit is no longer in 
their mouths.


@mes 地の文1
Her strength has been digested by the succubi, and even her 
flesh and blood were absorbed.
@mes Lilith
Mmmngh, Lilim... I think that's enough...
@mes Lilim
Yes, it looks like all of the spirit has joined with our bodies.
@mes Lilith
Ngh...\i[001]
@bgs -1
@val_set 2500000 int 0
@se sound/lip01.mp3
@common_ev 500281
@picture LU-li\li03.jpg 0 480 360 10
@wait 30
@se sound/lip02.mp3
@common_ev 500281
@picture LU-li\li09.jpg 0 480 360 10
@wait 70

@mes Lilim
Pch...\i[001]
#ピクチャぷよ
@toki-puyo 40300 40350 1
@se sound/eat2.mp3
@wait 70
@mes 地の文1
Their long kiss comes to an end.
With drool hanging off both of their lips, the two sisters give
one another lewd smiles and don't appear tired at all.

@se sound/power11.mp3
@val_set 2500000 int 1
@mes Lilith
Uhu... I'm overflowing with power.
The two of us are now one spirit, Lilim...
@mes Lilim
That's right, sister. 
No one can stop us now...
@mes Lilith
Let's paint the world in chaos and confusion, Lilim.
@mes Lilim
Yes, sister. We'll make it into our personal paradise.
@val_set 2500000 int 0
@toki-ef 4 0 60
@mes 地の文1
With the power of the wind, the powerful succubi succeed in 
plunging the world into a maelstrom of disorder.
@wait 80

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@toki-ef 4 0 -1

@mes 地の文1
.........
@wait 80


#メッセ終了
@mes_RESET