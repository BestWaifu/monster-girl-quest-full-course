############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Luka tatie/ブランク L
@setface Mimic tatie/ブランク L

@mes 地の文1
Why... Why did I carelessly approach that suspicious treasure 
chest...
By the time I started to wonder that, it was already too late.

@mes 地の文1
Just as I bent over to try to open it, the chest suddenly 
opened its mouth.

@common_ev 500281
@picture BF-mimi\cg1.jpg 0 480 360 10

@se sound/makituki.mp3
@toki-puyo 40300 40350 2
@se 10
@wait 70

@mes 地の文1
Roughly half my body was dragged inside before I was able to 
grab the edge and save myself.

#ピクチャぷよ

@mes Luka
Guh...! Let me out!

@se 10
@toki-puyo 40300 40350 2
#アニメ変数
@val_set 2500000 int 3

@mes 地の文1
Holding on for dear life, I desperately resist the monster 
trying to drag me further inside the chest.

@mes Mimic
Ahaha.♪ I didn't think there were any adventurers left these 
days who could be caught so easily.
Just relax and let me pull you all the way inside, ok?

@mes Mimic
I'll make you feel reallly good as I melt you into syrup and 
then eat every bit of you.
@mes Luka
You- you've got to be joking!


@mes 地の文1
The mimic continues to tempt me in a sweet voice.
However, I can hear the sloshing sounds of her slimy digestive 
fluids just waiting for her prey to fall into the chest to be 
digested.


@se 10

@mes 地の文1
I can't possibly surrender to that!
@mes Mimic
Hmm, you're a lot stronger than you look. 
Let me share a secret with you.

@mes Luka
Wh- what did you say...


@mes 地の文1
A secret? There's only one thing that comes to mind in this 
situation: it's a trap.
The moment I stop to listen to her, I bet she'll seize the 
opportunity.

@mes Luka
Don't waste your breath! 
You're just trying to catch me off guard! 
I'm not listening to you!

@mes Mimic
Well, it's supposed to be a secret... 
but it's just that, you know, I'm not really confident in 
my physical strength.
I'm probably not any stronger than a human girl, right?
@mes Mimic
The fact is that I'm just barely managing to keep you held down 
like this.

@mes Luka
Huh...?

#ウェイト
@wait 70

@mes 地の文1
That's an unexpected piece of information, but is it actually 
true? 
If she's telling the truth...

@mes 地の文1
As long as I don't give up, that means I still have a chance to 
escape. 
I have to gather all my strength and make an attempt at least!

@mes Mimic
There's one more secret I should tell you though.♪
I may be physically weak, but this chest has amazing shutting 
force.♪
It's strong enough to crush even an iron plate. 

@mes Mimic
With something like a human body... 
Well, it can easily slice that in half.♪

@se 16
#アニメ変数
@val_set 2500000 int 0
@common_ev 500281
@picture BF-mimi\cg2.jpg 0 480 360 10
@wait 70
@mes Luka
Wha...


@mes 地の文1
It has enough power to slice a human in two...?
Half my body is already trapped inside this terrible chest, 
though. 
That means... uh oh...

@common_ev 500281
@picture BF-mimi\cg3.jpg 0 480 360 10
@wait 70
@mes Mimic
Ahaha, did you figure it out?
Hey, hey... so which will it be?
Will you let your body be chomped in half in an instant or be 
slowly and gently dissolved inside my chest?

@val_set 2500000 int 1
@mes Luka
Ahhh, st- stop it... Help me...


@mes 地の文1
My face immediately goes pale. 
My life was always under this monster's control; the only 
question was how she was going to take it away. 
I've just been dancing to her tune with my hopes of escape.

@mes Mimic
Huh? What's wrong? 
Are you already done fighting against me?

@mes Luka
Tch!


@mes 地の文1
Calm down, Luka. 
Think. You have to think. 
There has to be some way out of this!

@mes Mimic
Hmmm, I see. 
You're going to ignore me.
Well, suit yourself. 
I'm going to eat you soon enough either way.

@se sound/kucha03.mp3
@common_ev 500281
@picture BF-mimi\cg4.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 1
@wait 70
@mes Luka
Hiii!


@mes 地の文1
A damp sensation splashes against my upper body: the slimy 
digestive juices contained inside the chest. 
The viscous liquid has begun climbing up my torso as if it has 
a mind of its own.

@mes Mimic
Aha.♪ That's it! 
This is only fun if I can get a reaction out of you.♪
Come on, don't you want to melt into me even faster?

#アニメ変数
@val_set 2500000 int 2
@mes Luka
Hiii... Stop, stop it...

@mes Mimic
I'm not gonna stop.♪ 
Ahaha.♪ Hora hora hora.
The lid will be closing in t-minus 10 seconds.♪
Are you prepared to die? 10, 9, 8...

@mes Luka
Hiiii, stop! Help me!


@val_set 2500000 int 3
@se 10

@mes 地の文1
Spurred by the mimic, I finally begin to panic.
Please no, I don't want to die!

@mes Mimic
7...6... nevermind, I'm bored already. 
Say bye-bye to this world.
Time to eat.♪

@mes Luka
Hiii! Nooo!

#アニメ変数
@val_set 2500000 int 0

@common_ev 500281
@picture BF-mimi\cg5.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 3
@se sound/close08.mp3
@wait 70

@mes 地の文1
*Slam*


@mes 地の文1
It happened in the blink of an eye. 
I thought about being dragged into the chest, and the instant I 
slackened my grip, the lid mercilessly closed over me.


@mes 地の文1
I doubt it's going to reopen either. 
At least not while I'm still alive...

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@mes 地の文1
......

@val_set 2500000 int 2
@bgs sound/roop.mp3
@common_ev 500281
@picture BF-mimi\cgb1.jpg 0 480 360 10
@wait 70
@mes Mimic
Ahahahaha.♪ The fun is just getting started, isn't it?
Now I'm going to melt you, even the hairs on your head, and 
slurp it allll up.
This is going to be great.♪
@mes Luka
Uwaaaa!


@mes 地の文1
The inside of the mimic's chest is filled with a light-blue 
digestive slime that splashes around my body in a wave-like 
motion.

@mes Mimic
Where do you want me to start? Your arms? Legs? 
Or maybe your dick?
I'll leave your head for last. 
It wouldn't be any fun to start there, after all.

@mes Luka
Hiii! St- stop... Let me out of here... haaa!


@se sound/ero_makituki2.mp3
@common_ev 500281
@picture BF-mimi\cgb2.jpg 0 480 360 10
@wait 70

@mes 地の文1
My lower body is suddenly assaulted with a thrilling sense of 
pleasure. 
The slime coils around my penis, giving it a comfortable 
stimulation.

@mes Mimic
Your girly voice is so cute.♪
You're feeling good even though your penis is being digested. 
What a pervert.♪

@mes Luka
No... ah, aahii!

@se sound/lick_s5.mp3

@mes 地の文1
It feels like the slime is chewing my penis. 
Even though my body is surely melting, the pleasant feelings 
are flooding me with an unexpected desire to ejaculate.

@mes Mimic
Here, let me melt all these clothes out of the way for you. 
Then I can make you feel even better.♪

@se 12
@common_ev 500281
@picture BF-mimi\cgb3.jpg 0 480 360 10
@wait 70
@mes Luka
Ah... st- stop...


@mes 地の文1
The mucus covering my body dissolves my clothing in the blink 
of an eye.
With my now naked skin exposed to this slippery sensation, the 
pleasure being given to me has increased manyfold. 

@mes 地の文1
There's no way I can withstand any more of this.

@mes Luka
Ah... I, I'm going to come...

@mes Mimic
Hehehe, that's fine. 
Let me have your sperm. 
You can shoot it all out while you melt. ♪

@mes Luka
No... ugh, coming...

@common_ev 500281
@picture BF-mimi\cgb4.jpg 0 480 360 10
@toki-ef 2 0
@wait 70
@mes 地の文1
Unable to bear the rising urge to ejaculate, sperm spill from 
my penis.

@mes Mimic
Aha. You came.♪ 
Your sperm are super delicious...♪
I really like you.♪ 
Hey, hey, give me more.♪

@bgs sound/roop.mp3
@mes Luka
Ah, hiaaa... stop... Stop teasing my penis...


@mes 地の文1
Even though I just came, her slime is already stimulating my 
penis more intensely than before.

@mes 地の文1
All at once, strength flows out of my body and leaves me 
feeling like I have no longer have any bones supporting my 
weight.

@mes Luka
Ah... no... I'm going to come again...

@mes Mimic
Ahaha.♪ 
It's all right. 
Keep giving it to me.♪

@common_ev 500281
@picture BF-mimi\cgb5.jpg 0 480 360 10

@toki-ef 2 0
@wait 70

@mes 地の文1
I come over and over while embraced by the mimic. 
With my body melting, I quiver in pleasure as I repeatedly 
ejaculate.

@mes 地の文1
Predator and prey. 
In this relationship, the mimic can torment me any way she 
likes.

@bgs sound/roop.mp3
@mes Mimic
Well then, I've waited long enough. 
It's meal time!♪
From your fingernails to the hair on your head, I'm going to 
melt them all to syrup and digest you. ♪

@mes Luka
Ah, hiiii... Help...

@mes Mimic
Toooo laaate.♪ 
Your body is already goo.
Doesn't being eaten alive feel great, though? 
You get to fully enjoy the final moments of your life.♪

@mes Luka
Hiaaa... It really does... feel wonderful...

@mes Mimic
Ahahahaha. 
You finally admit it.♪
You're so happy to have your body melted and absorbed into me.♪


@mes 地の文1
Succumbing to the mimic's provocative voice, I blissfully 
indulge in the pleasure of being eaten.

@mes Luka
Aaaah... so good... Eat more of me...

@mes Mimic
Ahaha, you've lost it.♪
Well, it looks like this is the end.
#アニメ変数
@val_set 2500000 int 2

@bgs sound/eat1.mp3
@common_ev 500281
@picture BF-mimi\cgb6.jpg 0 480 360 10
@wait 70

@mes 地の文1
The slime thickens and converges on my body as if to swallow 
the prey it has finished chewing. 
Without mercy, I'm digested inside the mimic.

@se 12

@mes Luka
Ah... aah...


@mes 地の文1
Bones, skin, heart, and even my soul are melted down. 
I don't even know who I am anymore.
While experiencing the pleasure of being preyed on by a monster, 
I meet my end as nutrients for the mimic.
#ウェイト
#アニメ変数
@val_set 2500000 int 0
@wait 70

@toki-ef 9

@bgs -1 100 100 120
@picture BF-mimi\cgb7.jpg 0 480 360 10
@se sound/eat2.mp3
@wait 120
@mes Mimic
Your body is allll gone now.♪
Thanks for the meal!♪

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@mes 地の文1
.........

#メッセ終了
@mes_RESET