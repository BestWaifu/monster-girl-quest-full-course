############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Cecil tatie/ブランク L
@setface Spider　Princess tatie/ブランク L

@mes Spider　Princess
Does it become difficult to move...?
Did you really thought it will be so easy...?

@mes Cecil
Guh.....no, not yet.....I still can.....!
@mes 地の文1
She is strong. Her power is overwhelming.
The chief of the Arachne race, Spider Princess.

@mes 地の文1
I can't defeat such a powerful opponent all by myself.

@mes 地の文1
Armor is getting peeled off every time sticky thread sticks 
to me, my opponent is still unharmed and there is no more armor 
to protect me.
@mes Cecil
No...!
Not yet..! I still have my sword...!
@mes Spider　Princess
Huh?...It's not that I dislike a struggling prey...
But...I got tired of watching your useless attempts already...
@se sound/swing.mp3
@mes Cecil
Hiya!!
@mes 地の文1
Holding my sword, I jumped at my opponent...and was about to 
stab her with it, but...!!

@mes Spider　Princess
It's time...can I have a bite of you now?

@mes Cecil
.....huh!!?

@toki-ef 0 0

@toki-shake 40300 40350 05 42
@common_ev 500281
@picture DE-km\km01.jpg 0 480 360 10

@se sound/makituki.mp3
@wait 70

@mes 地の文1
In the next moment, my body was covered in spider threads and 
my lower half was swallowed by Spider Princess's predatory 
mouth.

@mes Spider　Princess
Kukuku...I got you now...
You can't get away anymore...
So, what should I do with you...?

#アニメ変数
@val_set 2500000 int 2

@mes Cecil
Ah, aaaah...oh...no...!

@mes 地の文1
I understood that once my body was caught into this predatory 
mouth, I will surely die!
Since Spider Princess is quick, she was able to caught me in 
the blink of an eye.
@common_ev 500281
@picture DE-km\km02.jpg 0 480 360 10

@wait 70
@mes Cecil
Uh...guh...! Let...me...go!

@se sound/nukarumu.mp3
@toki-shake 40300 40350 05 42

@mes Spider　Princess
Kukuku...You really thought you could win...?
With that fragile body of yours...

@mes 地の文1
Spider Princess laughing at one who lost to her.
That smile is evil and brutal.
@common_ev 500281
@picture DE-km\km03.jpg 0 480 360 10

@wait 70
@mes Spider　Princess
But...that fragile body...
looks so soft...
I wonder what it will taste like?...kukuku...
@common_ev 500281
@picture DE-km\km04.jpg 0 480 360 10

@wait 70
@mes Cecil
Hiaa...t-taste...?!
@mes 地の文1
I'm just whetting her appetite.

@mes Spider　Princess
Well...how should I eat you...?
@common_ev 500281
@picture DE-km\km06.jpg 0 480 360 10

@wait 70
@mes Spider　Princess
Maybe bite you in the neck...
and suck out all of your bodily fluids without living a drop...
Sensation of sucking out someone's life is always great...

@mes Spider　Princess
Also, it's not a bad idea to tie you in a cocoon, and drink you 
when you are melted...
Kukuku...you even will be able to feel as your internal 
ingredients getting cooked...Are you worrying...?
@common_ev 500281
@picture DE-km\km05.jpg 0 480 360 10

@val_set 2500000 int 1
@wait 70
@mes Cecil
...Uh-huh.....

@mes 地の文1
My breath has stopped, and my mind and body was frozen because 
of this words.
I will be eaten for food by this monster.

@mes 地の文1
That fact has frozen my heart.
It was same feeling that prey feels when it's caught in 
spider's web.

@mes Spider　Princess
Okay...I think I'll just swallow you whole and digest you 
inside me...

@mes Spider　Princess
Kukuku...rejoice...
This way to eat is the slowest...
and it's also the most comfortable...
@mes Spider　Princess
It will be as comfortable as falling asleep inside your
mother's womb...
@common_ev 500281
@picture DE-km\km07.jpg 0 480 360 10

@wait 70
@mes Cecil
No...noooooooooo!!!


@val_set 2500000 int 3

@se sound/nukarumu.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
I'm going to be digested alive.
My brain makes me panic because of this words.

@se sound/nukarumu.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
However, I can't break the threads entangling my body, even 
though they are thin.
On the contrary, the more I move, the more of it digs into my 
body.

@mes Spider　Princess
Kukuku...Screaming and crying is useless.
Regretting now of raising up your blade at Spider Princess...?
Now I will slowly put your life into my womb...

@mes Cecil
No, no...stop...aaaaaah!!
@bgs sound/kutyu2.mp3

@se sound/sui.mp3
@common_ev 500281
@picture DE-km\km08.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 2
@wait 70

@mes 地の文1
My body sinks into her predatory vagina.
Since resistance is pointless, I can't do anything to prevent 
myself from getting swallowed.

@mes Spider　Princess
Kuku.....swallowing prey alive, this feeling....is incredible...


@mes 地の文1
Spider Princess speaks with excited voice.

@mes Spider　Princess
Tying flesh of others, taking control over them, eating them...
I am so happy I was born a monster...kukuku...

@mes 地の文1
That voice went through the bottom of my heart.
It's completely filled with pleasure.

@se sound/kucha03.mp3
@toki-shake 40300 40350 05 42
@common_ev 500281
@picture DE-km\km09.jpg 0 480 360 10

@wait 70

@mes Cecil
Uhhh.....nnn.....nnn.....

@se sound/kucha03.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
I'm struggling like a caterpillar that stuck in spider's web.
However, it's completely useless, I continuing my descent into 
Spider Princess's predatory mouth.

@mes Spider　Princess
Kukuku...how awkward...

@val_set 2500000 int 2
@se sound/sibaru.mp3
@picture DE-km\km10.jpg 0 480 360 10

@wait 70

@mes Cecil
Guh...the threads...won't coming...off...

@mes 地の文1
When I trying to get off the threads, it pressing on my neck 
and body even stronger, and slowing down my movements.

@mes Spider　Princess
You know...if you won't get out now, you will be swallowed...

@mes Cecil
Ah....ah.....no, no...!

@mes 地の文1
@se sound/sibaru.mp3
@picture DE-km\km11.jpg 0 480 360 10

@wait 70

@mes Spider　Princess
Kuku...I can't get tired of watching my prey struggling in 
despair...
Oh, that's even better now, when your arms are inside too...
Please... struggle some more...

@se sound/lick_s4.mp3
@toki-shake 40300 40350 05 42

@mes Cecil
Gyahh...haahh.....uh...guh...

@mes Spider　Princess
Kukuku...ten, nine, eight, seven...

@mes 地の文1
Spider Princess began to count.
It's to give even more despair to her prey who can't escape...
Countdown to death continues.

@val_set 2500000 int 1

@mes Cecil
Noooo...I can't get away.....
@mes Spider　Princess
Six...five...kukuku...this is the end...
@common_ev 500281
@picture DE-km\km12.jpg 0 480 360 10

@wait 70
@mes Cecil
Help......

@mes Spider　Princess
Two...one...

@mes Cecil
me――――


@bgs -1

@val_set 2500000 int 0

@se sound/eat2.mp3
@common_ev 500281
@picture DE-km\km13.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 2

@wait 70
@mes 地の文1
*Gulp*

@mes 地の文1
After my last scream...there was silcence.

@common_ev 500281
@picture DE-km\km14.jpg 0 480 360 10

@wait 70

@mes Spider　Princess
Kukuku...Well...can you still enjoy the fun...?
While melting inside me...enjoy your last moments...

@mes 地の文1
Spider Princess laughs in satisfaction.
That smile was full of cruelty and madness.

@val_set 2500000 int 1
@bgs sound/roop1.mp3
@common_ev 500281
@picture DE-km\kmb01.jpg 0 480 360 10

@wait 70

@mes Cecil
Uh...it's tying me...uh...hah...

@mes 地の文1
In the predatory organ of Spider Princess...is even more 
threads...
But this ones looks a bit different than the ones covering my 
body.

@mes Cecil
Ahhhh...it's covering my body...ah...
@se sound/nukarumu.mp3

@mes 地の文1
My body is gently entangled in spider's web.
A warm, damp threads are clinging to my body to stop me 
from resisting.

@se sound/lick_s4.mp3
@common_ev 500281
@picture DE-km\kmb02.jpg 0 480 360 10

@wait 70

@mes Cecil
Ah...My mind...is getting...vague...
What...is...this.....?

@mes 地の文1
It seems like when those other threads are covering my body, 
I'm getting weaker and it's getting harder to move.

@mes Cecil
No...way...is that...a poison...?
@mes 地の文1
But even though I know it, I can't escape from Spider Princess's 
body.
I can't do anything at all.


@val_set 2500000 int 2
@se sound/gulp_l.mp3
@toki-shake 40300 40350 05 42
@mes 地の文1
*Thump*

@mes 地の文1
Suddenly the space shaked.
@se sound/gulp_l.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
*Thump* *Thump*

@mes 地の文1
Spider Princess is moving.
Even though she ate me, she is already seeking for another prey.

@se sound/byoro01_a.mp3
@common_ev 500281
@picture DE-km\kmb03.jpg 0 480 360 10

@wait 70
@mes Cecil
Hiia...no...the threads...is tangling...me!

@mes 地の文1
Every time Spider Princess moves, my body hits against the 
walls of flesh, and even more threads seeps out, and there 
is nothing I can do to prevent it.
@se sound/gulp_l.mp3
@toki-shake 40300 40350 05 42
@mes 地の文1
*Thump*
@common_ev 500281
@picture DE-km\kmb04.jpg 0 480 360 10

@wait 70
@mes Cecil
No...stop...stop...

@mes 地の文1
My screams can't reach anyone.
And even if it could, it wouldn't change anything.
@se sound/kutyu3.mp3
@common_ev 500281
@picture DE-km\kmb05.jpg 0 480 360 10

@wait 70

@mes 地の文1
*Chew* *Chew*

@mes Cecil
Uh...faaa...it feels good...I...feel...better...

@mes 地の文1
Threads that covers my body taking away my thoughs and melting
my body and mind.

@mes Cecil
Aah...I...am...mel...ting...

@mes 地の文1
I experience pleasure from threads covering and melting my body.

@mes Cecil
Ufu...fu...awaahhh...mooooore...ah...

@mes 地の文1
Eventually, I started to move my body on purpose to envelop it 
in threads.
@toki-shake 40300 40350 05 42
@se sound/kucha03.mp3
@common_ev 500281
@picture DE-km\kmb06.jpg 0 480 360 10

@wait 70
@mes Cecil
Make me more...syrupy...

@mes 地の文1
Inside the Spider Princess's body, a prey is getting covered in 
threads while drowning in pleasure, so she can digest them more 
easily.
@se sound/kutyu4.mp3
@toki-shake 40300 40350 05 42
@mes Cecil
Ehehehe...it feels good...and comfortable...

@mes 地の文1
The more I move, the more of pleasant threads covers my body.

@mes 地の文1
A threads of death that dissolving, eroding, digesting the body
of the prey.
And it's also the sweetest and the most gentle thing possible.

@se sound/lick_s5.mp3
@toki-shake 40300 40350 05 42

@mes Cecil
Faaa...more...more...\i[001]

@mes 地の文1
My body is drowing in pleasure, as I covering myself in those 
threads just to satisfy myself.

@se sound/lick_s3.mp3
@toki-shake 40300 40350 05 42
@mes Cecil
Ahaaahh...wrap me up...moooooore...\i[001]

@se sound/gutyu0.mp3
@common_ev 500281
@picture DE-km\kmb07.jpg 0 480 360 10

@wait 70
@mes 地の文1
Every creature offered themselves willingly...
to become food for luscious Spider Princess.
@mes Cecil
NN...ag...haa...nn\i[001]

@mes 地の文1
Now when my whole body is wrapped in threads, I can't move even 
a single finger.

@se sound/kucha03.mp3
@mes Cecil
Nn...haa...so...sweet....


@se sound/nukarumu.mp3
@mes 地の文1
I can only breath with my mouth...
@mes Cecil
Aha...aahhh...ahhhhh...\i[001]


@mes 地の文1
My sensitive part is melting now too...

@bgs sound/inner1.mp3

@se sound/lick_s5.mp3
@common_ev 500281
@picture DE-km\kmb08.jpg 0 480 360 10

@wait 70
@mes Cecil
Nn...haa.....waah...


@val_set 2500000 int 1

@mes 地の文1
Now when my body is completely wrapped in the threads, I look 
just like a cocoon.


@mes Cecil
Awah...so...warm...
@mes 地の文1
Within the cocoon is a comfortable temperature, and it filled 
with a  mysterious sense of security, it's as if I was asleep 
in my mother's womb, I felt such an illusion.

@mes Cecil
Ah...fuh.....I...feel...drowsy...
@mes 地の文1
Unleashed from tension, the sensations in my body starts to 
getting weaker more and more.

@se 12

@mes 地の文1
I'm not scared, I accepted the things as they are.
@mes 地の文1
@se sound/mizu09.mp3
@common_ev 500281
@picture DE-km\kmb08.jpg 0 480 360 10

@wait 70
@mes Cecil
Nn...nnn...I'm...melting...nn...

@mes 地の文1
My body is melting in the threads with gurglings sounds.

@mes 地の文1
I'm melting alive.
Despite the horror of this act, I can feel only pleasure and 
peace.

@se sound/gutyu0.mp3
@mes 地の文1
*Chew* *Chew*
Slowly, like ice melts, my body becomes liquid.

@mes Cecil
Aah...haaaahhh...

@mes 地の文1
The sensations of my body disappears.
Bones, heart, and soul are melting away and disappearing.
@mes Cecil
I'm...so.....happy.....

@val_set 2500000 int 0

@bgs -1 100 100 120
#ウェイト
@wait 70

#画面エフェクト
#0フラッシュ　1黒フラッシュ　2射精　3画面振動　
#4完全暗転 （第二変数）は　0暗転　1白転　10立ち絵が見える暗転　-1解除　＋フレーム数
#5どきどき　6夜シーン用灯り表現　7画面RGB調整　8時間経過演出
#9長めフレームトランジション　10EV関連ピクチャ全消し
#11 ピクチャズーム P番　拡大率　フレーム
#12 選択肢前注意　13　イベント用天候変え　時間帯　0屋内1屋外
@toki-ef 9 0

@se 12
@se sound/gokkunn6.mp3
@picture DE-km\kmb10.jpg 0 480 360 10

@wait 140

@mes 地の文1
And like this, the body of Cecil the adventurer was covered in 
cocoon, and digested without leaving a trace.

@common_ev 500281
@toki-ef 4 0 10

@mes 地の文1
Thus, the girl who challenged the Queen Arachne, instead of 
fighting... was played with and turned into a simple meal...
@mes 地の文1
In the end she was swallowed and melted in pleasure, and her 
life has come to an end.

@wait 80

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@toki-ef 4 0 -1

@mes 地の文1
.........
@wait 80


#メッセ終了
@mes_RESET