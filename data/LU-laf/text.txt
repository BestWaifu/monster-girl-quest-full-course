############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Slug　Girl tatie/ブランク L
@setface Raffia　Canaan tatie/ブランク L

@mes 地の文1
The strong prey on the weak. That's the law of the jungle.
It doesn't matter whether you're human, animal, or monster.
Unfortunately, I currently find myself occupying the weak role 
in that truism.
@se sound/ero_makituki2.mp3
@common_ev 500281
@picture LU-laf\rh01.jpg 0 480 360 10
#ウェイト
@wait 70


@bgs sound/sea1.mp3

#アニメ変数
@val_set 2500000 int 2
@mes Slug　Girl
Eek! Let go of me!
@toki-shake 40300 40350 05 42
@se sound/attack00.mp3
@mes 地の文1
The vines binding my arms are tough enough that I can't break 
out of them and get free.
No matter how hard I pull, they don't yield an inch.
@mes Raffia　Canaan
Uhuhu, I'm not letting you go.
Well actually, I don't mind letting go...
However, if I do, your body is just going to drop into my 
digestive fluids and be melted to mush. Ok?
@mes Slug　Girl
Noooo! D-don't... Please spare me!
@common_ev 500281
@picture LU-laf\rh02.jpg 0 480 360 10
@wait 70
@mes Raffia　Canaan
You want to live? That's too bad. 
See, you're going into my pitcher plant where your flesh and 
bones will dissolve into my lunch.
@val_set 2500000 int 3
@mes Slug　Girl
I don't want to die!
@mes 地の文1
I had heard the rumors. The three Canaan sisters. 
Very skilled plant-type monsters.
More relevant to my current situation, each of them is said to 
be brutally cruel.

@mes 地の文1
If they're hungry, they won't limit their appetite to humans.
No, they'll attack monsters too, and insect-types should be 
especially wary of them.

@mes 地の文1
However, despite the warnings, I never thought I'd actually run
into one of them...

@mes Raffia　Canaan
Uhuhu... Your body's so slimy, it's hard to hold on to...
If I lose focus, I'm worried I might accidentally let you 
slip from my tentacles.
@val_set 2500000 int 2
@toki-shake 40300 40350 05 42
@se sound/attack00.mp3
@mes 地の文1
As her vines sway, my body shakes and dips further into the 
pool of digestive juices.
I can already feel random hot sensations from my lower half 
immersed in the tepid, honey-like fluid.

@mes 地の文1
It seems that I've begun to melt...
@common_ev 500281
@picture LU-laf\rh03.jpg 0 480 360 10
@wait 70
@mes Slug　Girl
S-stop! Someone help me!
@mes Raffia　Canaan
I'm sick of hearing you whine.
Will you entertain me a bit more and then please just relax and
become slug soup?
@mes Slug　Girl
Nooooo...!
@mes 地の文1
The predator and prey relationship is already fully established
between us, with me on the unfortunate end.
@mes Raffia　Canaan
Since I'm putting you through so much trouble, shall I let you 
enjoy yourself too?
Huhu... This will definitely make things more fun for me.
@mes Slug　Girl
Huh, what...? What are going to do?

@val_set 2500000 int 1
@bgs sound/inner1.mp3
@se sound/bubble6.mp3
@mes 地の文1
I'm afraid of not knowing what Raffia is up to. 
With a giggle and a smile on her face, the pitcher plant begins
bubbling and making waves.

@mes 地の文1
Moments later, I can feel the submerged part of my body heating
up.
@common_ev 500281
@picture LU-laf\rh04.jpg 0 480 360 10
@wait 70
@mes Slug　Girl
Ah... ah... Hot.. My body's...
Eeek, n-no... aaah...?
@mes 地の文1
The warmness is my lower body begins transforming into dull, 
throbbing feelings of pleasure just like the indulgent, obscene
sensations I experience when a human comes inside me.
@val_set 2500000 int 3
@common_ev 500281
@picture LU-laf\rh05.jpg 0 480 360 10
@wait 70
@mes Slug　Girl
Aaah! No, what are you doing...!
Eeek... Why does this feel so good...?
@mes Raffia　Canaan
I increased the concentration of aphrodisiacs in my digestive 
fluids for you.
After all, there's nothing better than slowly digesting someone
while watching her writhe in pleasure and beg to be melted.

@mes Raffia　Canaan
Hehe... Does that sound good to you?

@mes Slug　Girl
Eeeek! Ah, aaah!

@se sound/bubble6.mp3

@mes 地の文1
My lower half, smeared with slimy aphrodisiacs, is overwhelmed 
in pleasant feelings.
At the same time, a ruinous thought pops into my mind.

@mes 地の文1
That is, just how good would it feel for my whole body to be 
immersed in that liquid...
My small amount of remaining reason warns me that it would be 
suicide.

@mes 地の文1
Nevertheless, my body is already past the point of being able 
to resist the pleasure surging into it.
I can't help but want to experience ultimate ecstasy in 
exchange for my life.


@mes Slug　Girl
Hii... M-more... More...\i[001]
@mes 地の文1
I can't believe what I'm saying.
Controlled by pleasure, I'm actually begging for her to digest 
me.
@mes Raffia　Canaan
Uhuhu... I love honest and meek girls.
So will you ask me nicely? How about Miss Canaan, would you 
please melt me into syrupy nutrients and eat me?
@common_ev 500281
@picture LU-laf\rh03.jpg 0 480 360 10
@wait 70
@mes Slug　Girl
Ungh...!
@mes 地の文1
Swallowed by pleasure, my fate hangs on the next few seconds.
If I let her eat me, I'm obviously dead.

@mes 地の文1
I'm also faced with the reality that by sadistically trying to 
get me to ask for it myself, she's not only using me as a meal,
but also playing with my life for the fun of it.
@common_ev 500281
@picture LU-laf\rh05.jpg 0 480 360 10
@se sound/bubble6.mp3
@wait 70
@mes Slug　Girl
Hiiaah... M-Miss Ca...
@mes 地の文1
However, at this point I can't maintain control of my 
faculties.
My lower half is burning with even more heat than before, 
twisting and throbbing with pleasure in her pitcher plant.
@mes Raffia　Canaan
Yes? If you don't speak more clearly, I'm just going to leave 
you like this.
@mes Slug　Girl
Would you... please...
Melt me... Into syr...
@mes Raffia　Canaan
Hehe... I can't hear you.
@common_ev 500281
@picture LU-laf\rh06.jpg 0 480 360 10
@wait 70
@mes Slug　Girl
Syr... Syrupy nutrients and eat me!
@val_set 2500000 int 1
@mes 地の文1
Haha... In the end, I pronounced my death sentence from my own 
mouth... There's no salvation for me any longer.
@mes 地の文1
But I can't think of anything but drowning in pleasure right 
now anyway.
The only thing on my mind is wanting my whole body to be dunked
and immersed in this amazing joy.
@mes Raffia　Canaan
Hehe, well said. As you wish...

@val_set 2500000 int 0
@se sound/mizu1.mp3
@common_ev 500281
@picture LU-laf\rh07.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 2
@wait 70
@mes 地の文1
*Splash*!
@mes Raffia　Canaan
Please enjoy the intoxicating pleasure and the paradise that is
being digested by me...


@toki-ef 9 0

@bgs sound/roop1.mp3
@se sound/bubble6.mp3
@picture LU-laf\rh08.jpg 0 480 360 10

@toki-puyo 40300 40350 2
@wait 70
@mes Slug　Girl
Wha...? Ah... aaah!
@mes 地の文1
The shock of being suddenly dropped and having my entire body 
immersed in digestive juices brings me back to my senses and 
aware of my situation.

@se sound/Bubble16.mp3
@common_ev 500281
@picture LU-laf\rh09.jpg 0 480 360 10
@wait 70
@val_set 2500000 int 2
@mes Slug　Girl
Ah...? I... I'm... melting?
@mes Slug　Girl
Eeek... I'm being eaten!
@toki-puyo 40300 40350 2
@mes 地の文1
I frantically try to escape, but with nothing to grab ahold of,
my own mucus-covered body only slips off the slimy walls of her
pitcher plant.
@mes Slug　Girl
No, no way... huaaa...\i[001]

@se sound/Bubble16.mp3
@common_ev 500281
@picture LU-laf\rh10.jpg 0 480 360 10
@wait 70

@val_set 2500000 int 3
@mes 地の文1
However, in mere moments, the aphrodisiac digestive juices once
again melt away the desire to be anywhere else but here.
@mes Slug　Girl
Ah... ha... Slimy... Feels so... good... Nnngh...
@mes 地の文1
That's right. This is what I was looking forward to.
Having my entire body submerged in warm pleasure takes away my 
will to resist.
@mes Slug　Girl
Ngh... I'm so horny...
Ah... C-coming... I'm coming again... ah...
@toki-puyo 40300 40350 2
@se sound/Bubble16.mp3
@common_ev 500281
@picture LU-laf\rh10.jpg 0 480 360 10
@wait 70
@val_set 2500000 int 1
@mes 地の文1
Caressed by the smooth walls of the pitcher plant and coated in
aphrodisiacs, I climax over and over as I gradually lose 
feeling in my body.
@mes Slug　Girl
Ah... ah...
@mes 地の文1
In seconds, I'm so addicted to the pleasure that I can't even 
speak properly.
In my vacant trance, I watch my body begin the process of 
dissolving.
@se 12
@se sound/mizu09.mp3
@common_ev 500281
@picture LU-laf\rh11.jpg 0 480 360 10
@wait 70
@mes Slug　Girl
I'm... melting... ah...\i[001]
@mes 地の文1
With a constant burbling sound, my cellular structure starts to
unravel.
Her virulent digestive fluids are making short work of my body.
@mes Slug　Girl
Ah... Ngh...\i[001]
@mes 地の文1
My mind is scrambled by aphrodisiacs, and my body is melted by 
digestive juices. 
As I repeatedly orgasm, my existence fades from this world.
@se sound/mizu09.mp3
@se 12
@common_ev 500281
@picture LU-laf\rh12.jpg 0 480 360 10
@wait 70
@mes Slug　Girl
Ha... ah...\i[001]
@mes 地の文1
However, even that sensation is supremely satisfying to me 
right now.
@mes Slug　Girl
So... ha...ppy...\i[001]

@se sound/Bubble16.mp3
@mes 地の文1
*Glug*
@se sound/Bubble16.mp3
@mes 地の文1
*Glug*
@se sound/mizu09.mp3
@val_set 2500000 int 0


@wait 70

@se 12
@toki-ef 9 0

@picture LU-laf\rh13.jpg 0 480 360 10
@wait 120
@mes 地の文1
*Gurgle*

@bgs -1 100 100 120
@se sound/eat2.mp3
@toki-ef 4 0 10

@mes 地の文1
......
@toki-ef 4 -1 100
@se sound/bubble6.mp3
@common_ev 500281
@picture LU-laf\rh14.jpg 0 480 360 10
@wait 70
@mes Raffia　Canaan
Uhuhu, thanks for lunch.
Getting prey addicted to pleasure and peacefully digesting them
really does make for the best meals.
@toki-ef 4 0 10
@mes 地の文1
Thus, in accordance with the survival of the fittest, the weak 
girl was eaten by the stronger one.
In the world of monsters, this natural law reigns supreme.
@wait 80

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@toki-ef 4 0 -1

@mes 地の文1
.........
@wait 80


#メッセ終了
@mes_RESET